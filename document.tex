\documentclass[10pt,a4paper,oneside]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage[left=3.5cm,right=3.5cm,top=3.5cm,bottom=3.5cm]{geometry}
\setlength{\parskip}{3mm}
\setlength{\parindent}{0cm}
\title{A Line Based Approach for Bugspots}
\author{Maximilian Scholz}
\newcommand*{\blankpage}{
  \vspace*{\fill}
  \begin{flushright}
  \tiny THIS PAGE INTENTIONALLY LEFT BLANK.
  \end{flushright}
  \pagebreak
}
\begin{document}
\maketitle

\pagebreak

\section*{Introduction to Bugspots}

Bugspots were proposed by a Google blog post \cite{google_bugspots}. The idea behind it is to look at the commit history of a file and give the file a "bug prone" score.
A higher score means it is more important to review the file if changed as it seems to contain a lot of bugs. This is important for bigger code review processes as there might just not be the manpower or time to review everything, in this case the scores can be used to form a priority list. Even with enough time to review everything, it can be nice to know if a file had bugs in its past, so the reviewer can be more careful.

\subsection*{The Original Algorithm}

The algorithm is rather simple. It takes all commits made to a file back to a set date. From this set it extracts the commits that fixed bugs, usually indicated by some kind of "fixes" keyword in the commit message. Here it can be useful to double check if the fix really was a bug or just a feature request.

The commits are then weighted based on their age, so that older commits weight less then newer ones. The formula used by the Google engineers was the following:
$$Score = \sum_{i=0}^{n}\frac{1}{1+e^{-12t_i+12}}$$
Where $n$ is the number of bug-fixing commits and $t_i$ is the timestamp of the bug-fixing commit represented by i. The timestamps are normalized from 0 to 1, where 0 is the moment you went back to start from and 1 is when the algorithm is run.

\section*{The Goal}

The goal of this thesis is to investigate if it is useful to run a bugspot algorithm on single lines of code instead of whole files.

\section*{Evaluation Methods}

There are two ways to evaluate how good the algorithms perform.

The first one is to run the algorithm on a state in the past, say half a year ago, and then compare the results of the algorithm with actual fixes that happened since then. The better the hot-spots match the reported bugs or bug-fixes the better the algorithm performs.

The other way is to set up repositories so they use the algorithms in their work flow. In this case GitMate will be used to do this with the coala \cite{coala} and coala-bears \cite{coala-bears} repositories. Besides comparing the hot-spots with bug reports or fixes this way can also generate feedback from the developers. Still the practical approach might not be feasible as the time available for testing is not very long.

For comparing the file and line based algorithms the file based results will be transformed to a line based result, giving every line the score of the file. This way it can be determined if the line based algorithm performs better then the file based one. 
An enhanced version of the line based algorithm could add a Gaussian distribution of the area around every line to the line's score to emphasize accumulations of bugspots.

\section*{Development Environment}

The code for this thesis will be developed in python. I will use a bugspots implementation which is maintained by GitMate \cite{gitmate} as my starting point.
For testing and evaluating I will use the coala \cite{coala} and coala-bears \cite{coala-bears} repositories.

\newpage
\section*{Schedule}

This section holds milestones for each week and a short description of what to work on.

\subsection*{1st Week - Learn the Algorithm (week 26)}

Measurable Goals:

\begin{enumerate}
\item Additional repositories to test and evaluate the algorithm against are chosen. Preferably matching to similar work \cite{bugcache} \cite{bugspots_alternative1} \cite{bugspots_alternative2} .
\item The reasoning for choosing above is written down preliminarily.
\item The algorithm is defined.
\item The Data structures for storing the lines and scores are chosen.
\item The reasoning for choosing above is written down preliminarily.
\item The development environment is set-up.
\item Implementation of the line based algorithm has begun.
\item Goals for at least the next week are re-evaluated.
\item Update is sent to professor Schupp. 
\end{enumerate}

\subsection*{2nd Week - Implement the Algorithm (week 27)}

Measurable Goals:

\begin{enumerate}
\item The bugspots algorithm on lines is implemented.
\item A description of the algorithm is written down preliminarily.
\item A sanity check is made for the algorithm.
\item The result of the check is written down preliminarily.
\item Goals for at least the next week are re-evaluated.
\item Update is sent to professor Schupp. 
\end{enumerate}


\subsection*{3rd Week - Enhance the Algorithm (week 28)}

Measurable Goals:

\begin{enumerate}
\item Both algorithms are applied to the first test repository.
\item The result is written down preliminarily.
\item The Gaussian distribution modification for the line based algorithm is implemented.
\item Goals for at least the next week are re-evaluated.
\item Update is sent to professor Schupp. 
\end{enumerate}


\subsection*{4th Week - Use the Algorithm and Start Writing (week 31)}

Measurable Goals:

\begin{enumerate}
\item The line based algorithm is implemented into GitMate to be usable in production.
\item The line based algorithm will be used for coala \cite{coala} and the original one for coala-bears \cite{coala-bears} for the next 2 weeks.
\item Both algorithms are applied to the second test repository.
\item The result is written down preliminarily.
\item Goals for at least the next week are re-evaluated.
\item Update is sent to professor Schupp. 
\end{enumerate}

\subsection*{5th Week - The First Test (week 32)}

Measurable Goals:

\begin{enumerate}
\item Both algorithms are applied to the third test repository.
\item The result is written down preliminarily.
\item An intermediate presentation is prepared.
\item Goals for at least the next week are re-evaluated.
\item Update is sent to professor Schupp. 
\end{enumerate}

\subsection*{Intermediate Presentation (week 35)}

The intermediate presentation will cover everything achieved by this date. It includes specifically:

\begin{enumerate}
\item An introduction to the topic.
\item An introduction to the algorithm.
\item A presentation and short evaluation of results for the different algorithms so far.
\end{enumerate}

\subsection*{6th Week - Compare it to Others (week 35)}

Measurable Goals:

\begin{enumerate}

\item The results are compared to the results of other papers and the evaluation written down in the thesis.
\item Goals for at least the next week are re-evaluated.
\item Update is sent to professor Schupp. 
\end{enumerate}

\subsection*{7th Week - Buffer (week 36)}

This week works as a buffer as it is quite normal to underestimate the time needed for projects of this scope.

If there is time left, it can be used to work on a way to rank pull requests on a "how careful do I have to review" scale.


\subsection*{8th and 9th Week - Writing (week 37 and 38)}

Measurable results:

\begin{enumerate}
\item Everything is written down.
\item The thesis is finished.
\end{enumerate}

\begin{thebibliography}{999}
\bibitem {google_bugspots} \url{http://google-engtools.blogspot.de/2011/12/bug-prediction-at-google.html}
\bibitem {bugspots_alternative1} \url{http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=7273434}
\bibitem {bugspots_alternative2} \url{http://se.cite.ehime-u.ac.jp/~aman/pdf/IWESEP2013.pdf}
\bibitem {bugcache} \url{https://dl.acm.org/citation.cfm?id=2025157}
\bibitem {code_churn} \url{http://research.microsoft.com/pubs/69126/icse05churn.pdf}
\bibitem {gitmate} \url{https://gitlab.com/gitmate/bugspots3}
\bibitem {coala} \url{https://github.com/coala-analyzer/coala}
\bibitem {coala-bears} \url{https://github.com/coala-analyzer/coala-bears}

\end{thebibliography}
\end{document}